#!/usr/bin/python3

import sys
import re
import requests
import urllib

# Usage: sentinel2-ingest <call-id> <supervisor-endpoint> <catalog> [<param>]*
#   * <call-id>: Call ID
#   * <supervisor-endpoint>: Base URL of supervisor API
#   * <catalog> Target catalog to be queried for products
#       Possible values:
#       - creodias
#   * <param>: key=value pair for the query (multiple pairs possible)
#       Possible keys:
#       - pl (L1C or L2A)
#       - aoi (value must be WKT)
#       - start (value must be ISO-8601-formatted time)
#       - end (value must be ISO-8601-formatted time)


s2_identifier_regex = re.compile(r"(?P<p>S2[AB])_MSI(?P<pt>(L1C|L2A))_(?P<dt>\d{8}T\d{6})_N(?P<bl>\d{4})_R(?P<relorbit>\d{3})_T(?P<tile>.{5})_\d{8}T\d{6}")

def query_creodias(query_params, callback):
    """Queries the CREODIAS catalog with the given parameters
    and performs an action for each self reference link.

    Parameters
    ----------
    query_params : dict
        Key/value pairs of original parameters (from command line).
    callback : function
        A function taking one parameter (the self referene link)
    """

    map = {
        'pid': 'productIdentifier',
        'pt': 'productType',
        'aoi': 'geometry',
        'start': 'startDate',
        'end': 'completionDate',
        'cc': 'cloudCover'
    }
    count = 0
    items_per_page = 100
    total_results = None
    page = 1

    cat_params = {
        'sortParam': 'startDate',
        'sortOrder': 'ascending',
        'maxRecords': str(items_per_page),
        'exactCount': '1',
        'page': '1'
    }
    if 'uid' in query_params:
        uid = query_params['uid']
        query_params['pid'] = "/eodata/Sentinel-2/MSI/L2A/{1}/{2}/{3}/{0}.SAFE".format(uid, uid[11:15], uid[15:17], uid[17:19])

    for key, value in query_params.items():
        if key in map:
            cat_params[map[key]] = value

    print("Query URL: https://finder.creodias.eu/resto/api/collections/Sentinel2/search.json?{0}".format('&'.join([ "{0}={1}".format(p, cat_params[p]) for p in cat_params])), file=sys.stderr)

    # Initialize method-specific object
    method_object = callback(None, None)

    # Query page by page
    while total_results is None or count < total_results:
        response = requests.get(
            "https://finder.creodias.eu/resto/api/collections/Sentinel2/search.json",
            params=cat_params
        )

        result = response.json()
        response.close()

        total_results = result['properties']['totalResults']
        features = result['features']
        if page == 1:
            print("Total results (expected): {0}".format(total_results), file=sys.stderr)

        for feature in features:
            count += 1
            callback(feature, method_object)

        print("Processed so far: {0}".format(count), file=sys.stderr)

        if len(features) < items_per_page:
             break

        page += 1
        cat_params['page'] = str(page)

    print("Total results (final): {0}".format(total_results), file=sys.stderr)

    # Do final activity on method-specific object
    callback(None, method_object)


def analyze_tiles(metadata, obj):
    """Analyzes the query result and writes relevant information to a local file.

    Parameters
    ----------
    metadata : object
        The JSON feature object from the catalogue.
    obj : object
        A method-specific object that may be updated during each call.
    """

    # For initial call, metadata and obj are None, as a trigger to initialize
    # the method-specific object
    # For the final call, metadata is None and obj has a value, as a trigger to
    # do the final activity (output).
    if metadata is None:
        if obj is None:
            # Initialize (obj will be a dictionary and updated for each item in the search result)
            return {}
        else:
            # Print the aggregated output
            for area in sorted(obj):
                print(
                    "Tile/relative orbit: {0}, products: {1: 3}, best: {2} (cc = {3}%), footprint: {4}".format(
                        area,
                        obj[area]['count'],
                        obj[area]['best_product'],
                        obj[area]['best_cc'],
                        obj[area]['best_fp'],
                    )
                )
            return obj

    properties = metadata['properties']
    identifier = properties['title'].replace(".SAFE", '')

    print(identifier, end='', file=sys.stderr)
    match = s2_identifier_regex.match(identifier)
    if not match:
        print(" -> ERROR")
        return
    area = "{0}/{1}".format(match.group('tile'), match.group('relorbit'))
    cloud_cover = round(properties['cloudCover'], 2)

    geometry = metadata['geometry']
    if geometry['type'] == 'Polygon':
        footprint_len = len(geometry['coordinates'][0])
        wkt = "POLYGON("
        for (i, ring) in enumerate(geometry['coordinates']):
            if i != 0: wkt += ","
            wkt += "("
            for (j, point) in enumerate(ring):
                if j != 0: wkt += ","
                wkt += "{0} {1}".format(point[0], point[1])
            wkt += ")"
        wkt += ")"

    print(" -> {0} cc={1} polygon={2} wkt={3}".format(area, cloud_cover, footprint_len, wkt), file=sys.stderr)

    if area in obj:
        obj[area]['count'] += 1
        if cloud_cover < obj[area]['best_cc'] or cloud_cover == obj[area]['best_cc'] and footprint_len < obj[area]['best_fp']:
            obj[area]['best_product'] = identifier
            obj[area]['best_cc'] = cloud_cover
            obj[area]['best_fp'] = footprint_len
    else:
        obj[area] = {
            'count': 1,
            'best_product': identifier,
            'best_cc': cloud_cover,
            'best_fp': footprint_len
        }





def ingest_import(metadata, obj):
    """Ingests the reference via the supervisor API (import-acquisition).

    Parameters
    ----------
    metadata : object
        The JSON feature object from the catalogue.
    obj : object
        A method-specific object that may be updated during each call.
    """

    # For initial call, metadata is None, as a trigger to initialize
    # the method-specific object (not used here)
    if metadata is None:
        return

    properties = metadata['properties']
    identifier = properties['title'].replace(".SAFE", '')
    self_url = next((link['href'] for link in properties['links'] if link['rel'] == 'self'), None)

    print(identifier, end='', file=sys.stderr)

    if not self_url:
        print(" -> NO LINK", file=sys.stderr)
        return False

    ingest_url = "{0}/calls/{1}/import-acquisition?importUrl={2}".format(
        supervisor_endpoint,
        call_id,
        urllib.parse.quote(self_url)
    )

    print(" -> {0}".format(ingest_url), end='', file=sys.stderr)

    if test:
        print(" -> TEST", file=sys.stderr)
        return True

    # POST to ingestion URL
    response = requests.post(
        ingest_url,
        auth=supervisor_credentials
    )
    print(" -> {0}".format(response.status_code), file=sys.stderr)

    return response.status_code < 300



def ingest_cos2(metadata, obj):
    """Ingests the reference via the supervisor API (cos2 notification).

    Parameters
    ----------
    metadata : object
        The JSON feature object from the catalogue.
    obj : object
        A method-specific object that may be updated during each call.
    """

    # For initial call, metadata is None, as a trigger to initialize
    # the method-specific object (not used here)
    if metadata is None:
        return

    properties = metadata['properties']
    identifier = properties['title'].replace(".SAFE", '')
    agency = "ESA"
    satellite = properties['platform'].replace("S", "SENTINEL_")
    mission = "SENTINEL_2"
    start_time_str = properties['startDate']
    end_time_str = properties['completionDate']

    self_url = next((link['href'] for link in metadata['properties']['links'] if link['rel'] == 'self'), None)
    download_url = properties['services']['download']['url']
    thumbnail_url = properties['thumbnail']
    thumbnail_xml = f"<thumbnailFileUrl>{thumbnail_url}</thumbnailFileUrl>" if thumbnail_url else ''

    print(identifier, end='', file=sys.stderr)

    if not self_url:
        print(" -> NO LINK", file=sys.stderr)
        return False

    if download_url is None:
        print(" -> NO DOWNLOAD", file=sys.stderr)
        return False


    footprint = ""
    for ring in metadata['geometry']['coordinates']:
        for point in ring:
            footprint += "        <polyPoint>\n            <latitude>{1}</latitude>\n            <longitude>{0}</longitude>\n        </polyPoint>\n".format(
                point[0],
                point[1]
            )



    content = f"""<?xml version="1.0" encoding="utf-16"?>
<acquisition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <identifier>{call_id}-{agency}-{satellite}-{identifier}</identifier>
    <callId>{call_id}</callId>
    <recordCreated>2022-06-25T09:02:38Z</recordCreated>
    <recordUpdated>2022-06-25T09:03:04Z</recordUpdated>
    <agency>{agency}</agency>
    <satellite>{satellite}</satellite>
    <satelliteMission>{mission}</satelliteMission>
    <state>PRODUCT_EXTERNAL</state>
    <payload>MSI</payload>
    <resolution>MR</resolution>
    <sensorType>O</sensorType>
    <acquisitionStartDate>{start_time_str}</acquisitionStartDate>
    <acquisitionEndDate>{end_time_str}</acquisitionEndDate>
    <!--<quicklookFileUrl>put the link to the quicklook on store</quicklookFileUrl>-->
    {thumbnail_xml}
    <acquisitionFileUrl>{download_url}</acquisitionFileUrl>
    <acquisitionFileSize xsi:nil="true" />
    <noradId>99999</noradId>
    <metadataFileUrl>{self_url.replace('&', '&amp;')}</metadataFileUrl>
    <footprint>
{footprint}
    </footprint>
    <isProductPrivate>false</isProductPrivate>
</acquisition>"""

    ingest_url = "{0}/cos2".format(
        supervisor_endpoint
    )

    print(" -> {0}".format(ingest_url), end='', file=sys.stderr)

    if test:
        print(" -> TEST", file=sys.stderr)
        print("--------", file=sys.stderr)
        print(content, file=sys.stderr)
        print("--------", file=sys.stderr)
        return True

    # POST to ingestion URL
    response = requests.post(
        ingest_url,
        auth=supervisor_credentials,
        data={
            'createNew': 'true',
            'xmlContent': content
        }
    )
    print(" -> {0}".format(response.status_code), file=sys.stderr)

    return response.status_code < 300



if len(sys.argv) < 5:
    print("""Usage: sentinel2-ingest [key=value]*

Arguments:
    call=<call-id>
    catalog=<catalog> (creodias, default: creodias)
    endpoint=<supervisor-endpoint>
    credentials=<supervisor-credentials>
    method=<method> (analyze, import or cos2, default: cos2)
    uid=<product-uid>
    pt=<product_type> (L1C or L2A)
    aoi=<aoi-wkt>
    start=<start-time>
    end=<end-time>
    cc=<cloud-cover> (use interval syntax, e.g. '20]' for cc <= 20)
    test=1 (enable test mode, no import)
""",
        file=sys.stderr
    )
    sys.exit(1)

# Get arguments from command line
call_id = None
catalog = 'creodias'
supervisor_endpoint = None
supervisor_credentials = None
ingestion_method='cos2'
test = False

key_value_regex = re.compile(r"(?P<key>.+?)=(?P<value>.*)")
query_params = {}
for param in sys.argv[1:]:
    match = key_value_regex.match(param)
    if match:
        key = match.group('key')
        value = match.group('value')
        print(" - {0} = {1}".format(key, value), file=sys.stderr)
        if key == 'call':
            call_id = value
        elif key == 'catalog':
            catalog = value
        elif key == 'endpoint':
            supervisor_endpoint = value
        elif key == 'credentials':
            supervisor_credentials = tuple(value.split(':', 1))
        elif key == 'method':
            ingestion_method=value
        elif key == 'test':
            test=True
        else:
            query_params[key] = value

try:
    if not call_id:
        raise Exception("Call ID missing")
    if not catalog:
        raise Exception("Catalog missing")
    if not supervisor_endpoint:
        raise Exception("Supervisor endpoint missing")

    # Ingestion method
    if ingestion_method == 'analyze':
        ingestion_method = analyze_tiles
    elif ingestion_method == 'import':
        ingestion_method = ingest_import
    elif ingestion_method == 'cos2':
        ingestion_method = ingest_cos2
    else:
        raise Exception("Unsupported ingestion method")

    # Query and ingest
    if catalog == 'creodias':
        query_creodias(query_params, ingestion_method)
    else:
        raise Exception("Unsupported catalog")

except Exception as e:
    print("ERROR: {0}".format(str(e)), file=sys.stderr)
    if test:
        raise
    sys.exit(1)


